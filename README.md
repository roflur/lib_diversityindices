# R package _libdiversityindices_

calculates multidimensional functional diversity indices after https://esajournals.onlinelibrary.wiley.com/doi/full/10.1890/07-1206.1

  *  note, the respective functions work with three layers of data only - for now..
