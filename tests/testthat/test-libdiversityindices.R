# rm(list = ls()); options(device = 'X11')
context("diversityindices")

set.seed(123)
n <- 100
Test <- matrix(rpois(n*n,lambda = 20), n, n)
Test <- (Test + abs(min(Test, na.rm = TRUE)))/max(Test + abs(min(Test, na.rm = TRUE)), na.rm = TRUE)
Test <-  Test - mean(Test, na.rm = TRUE)

xData <- Test
yData <- Test + matrix(rnorm(n*n), n, n)
zData <- Test + matrix(rnorm(n*n, 10, 4), n, n)
lambda <- 20

test_mean     <- fdiversityindeces(l = lambda, xData, yData, zData, index = "mean", no_cores = 1)
test_richness <- fdiversityindeces(l = lambda, xData, yData, zData, index = "richness", no_cores = 1)
test_evenness <- fdiversityindeces(l = lambda, xData, yData, zData, index = "evenness", no_cores = 1)
test_divergence <- fdiversityindeces(l = lambda, xData, yData, zData, index = "divergence", no_cores = 1)

load("indicetest.ROut.save")

test_that("indices are stable", {
  expect_equal(test_mean, indices[["mean"]])
  expect_equal(test_richness, indices[["richness"]])
  expect_equal(test_evenness, indices[["evenness"]])
  expect_equal(test_divergence, indices[["divergence"]])
})

# -- save output in case the example is changed ----------------------------------- #
# indices <- list(mean = test_mean,
#                 richness = test_richness,
#                 evenness = test_evenness,
#                 divergence = test_divergence)
#
# save(indices, file = "indicetest.ROut.save")

# -- some plots ------------------------------------------------------------------- #
  # par(mfrow = c(2,4))
  # fields::image.plot(test_mean)
  fields::image.plot(test_richness)
  # fields::image.plot(test_evenness)
  # fields::image.plot(test_divergence)
  # fields::image.plot(xData)
  # fields::image.plot(yData)
  # fields::image.plot(zData)
# --------------------------------------------------------------------------------- #

# -- profiling -------------------------------------------------------------------- #
# profvis::profvis(libdiversityindices::fdiversityindeces(l = lambda, xData, yData, zData, index = "evenness"))

# # enable profiling
# utils::Rprof()
# # Run the code to be profiled
# test_richness <- libdiversityindices::fdiversityindeces(l = lambda, xData, yData, zData, index = "richness")
# # Disable profiling
# utils::Rprof(NULL)
#
# # Read the profiling results and view
# res <- utils::summaryRprof()
# res[["by.self"]]
# --------------------------------------------------------------------------------- #