# set.seed(123)
# n <- 100
# Test <- matrix(rpois(n*n,lambda = 20), n, n)
# Test <- (Test + abs(min(Test, na.rm = TRUE)))/max(Test + abs(min(Test, na.rm = TRUE)), na.rm = TRUE)
# Test <-  Test - mean(Test, na.rm = TRUE)
#
# xData <- Test
# yData <- Test + matrix(rnorm(n*n), n, n)
# zData <- Test + matrix(rnorm(n*n, 10, 4), n, n)
# lambda <- 5
#
# test_mean     <- fdiversityindeces(l = lambda, xData = xData, yData = yData, zData = zData, index = "mean", no_cores = 1)
# test_richness <- fdiversityindeces(l = lambda, xData, yData, zData, index = "richness", no_cores = 1)
# test_evenness <- fdiversityindeces(l = lambda, xData, yData, zData, index = "evenness", no_cores = 1)
# test_divergence <- fdiversityindeces(l = lambda, xData, yData, zData, index = "divergence", no_cores = 1)
# test_dbfd     <- fdiversityindeces(l = lambda, xData, yData, zData, index = "dbFD", no_cores = 1)
#
# test <- cbind(c(xData), c(yData), c(zData))
# rownames(test) <- as.character(1:nrow(test))
#
#
# par(mfrow = c(2,4))
# fields::image.plot(test_mean)
# fields::image.plot(test_richness)
# fields::image.plot(test_evenness)
# fields::image.plot(test_divergence)
# fields::image.plot(xData)
# fields::image.plot(yData)
# fields::image.plot(zData)
