# --------------------------------------------------------------------------------- #
#' pad NA's around matrix
#' @param vSample matrix
#' @param lambda radius
#'
#' @export
padperimeter <- function(vSample, lambda) {
  padded <- cbind(matrix(NA, nrow = nrow(vSample), ncol = lambda),
                  vSample,
                  matrix(NA, nrow = nrow(vSample), ncol = lambda))
  padded <- rbind(matrix(NA, ncol = dim(padded)[2], nrow = lambda),
                  padded,
                  matrix(NA, dim(padded)[2], nrow = lambda))
  return(padded)
}

#' create logical circle (matrix)
#' @param lambda raduis for circle.
#'
#' @export
createradius <- function(lambda = numeric()) {
  radius <- seq(from = -lambda:lambda)
  n <- length(radius)
  m_radiusindices <- matrix(TRUE, ncol = n, nrow = n)

  M <- c(lambda+1, lambda+1)

  for (i in 1:floor(n/2)) {
    for (j in 1:floor(n/2)) {
      if (sqrt((M[1] - i)^2 + (M[2] -j)^2) > lambda) {
        m_radiusindices[i, j] <- FALSE
      }
    }
  }
  for (i in floor(n/2):n) {
    for (j in floor(n/2):n) {
      if (sqrt((M[1] - i)^2 + (M[2] -j)^2) > lambda) {
        m_radiusindices[i, j] <- FALSE
      }
    }
  }

  # in l2
  # M <- 2*(lambda + 1)
  #
  # for (i in 1:floor(n/2)) {
  #   for (j in 1:floor(n/2)) {
  #     if (abs(M - (i + j)) > lambda) {
  #       m_radiusindices[i, j] <- FALSE
  #     }
  #   }
  # }
  # for (i in floor(n/2):n) {
  #   for (j in floor(n/2):n) {
  #     if (abs(M - (i + j)) > lambda) {
  #       m_radiusindices[i, j] <- FALSE
  #     }
  #   }
  # }

  m_radiusindices <- m_radiusindices & m_radiusindices[n:1,]
  return(m_radiusindices)
}


#' Calculate Functional richness
#' @param X three dimensional array
#'
#' @export
#'
#' @importFrom geometry convhulln
FRich <- function(X) {
  return(geometry::convhulln(X, options = list('FA'))$vol)
}

#' Calculate Functional evenness
#' @param X three dimensional array
#'
#' @export
#'
#' @importFrom BBmisc suppressAll
#' @importFrom emstreeR mlpack_mst
FEve <- function(X) {
  from_to_matrix <- BBmisc::suppressAll(emstreeR::mlpack_mst(scale(X)))
  EWl <- c(from_to_matrix[3, ], 0)
  S <- length(EWl)

  # PEW
  sumEWls <- sum(EWl)
  PEWls <- EWl/sumEWls

  subtractor <- 1/(S -1)
  mins <- sapply(PEWls, function(x) { min(x, subtractor, na.rm = TRUE) } )
  return((sum(mins) - subtractor)/(1 - subtractor))
}

#' Calculate Functional diversity
#' @param X three dimensional array
#'
#' @export
#'
#' @importFrom  SDMTools COGravity
FDiv <- function(X) {
  S <- nrow(X)
  cog <- SDMTools::COGravity(X[,1], X[,2], X[,3])[c(1, 3, 5)]
  names(cog) <- NULL

  dGi <- rep(NA, S)
  dGi <- sapply(1:S, function(x) { sqrt(sum((cog - X[x, ])^2)) })
  dG <- mean(dGi)
  deltad <- sum(1/S* abs(dGi - dG))

  return(dG/(deltad + dG))
}
#####################################################################################


#####################################################################################

#' Calculate Functional indicies, moving window
#' @param l lambda, i.e. window size
#' @param xData matrix
#' @param yData matrix
#' @param zData matrix
#' @param index functional diversity indice \code{"mean", "richness", "evenness", "divergence"}.
#' @param no_cores specifiy the number of cores which should be used.

#' @export
#'
#' @importFrom parallel detectCores
#' @importFrom parallel makeCluster
#' @importFrom parallel clusterExport
#' @importFrom parallel parSapply
#' @importFrom FD dbFD
fdiversityindeces <- function(l, xData, yData, zData, index, no_cores = NULL) {
  lambda <- l

  m_radiusindices <- createradius(lambda)
  transformedsignal1 <- padperimeter(vSample = xData, lambda = lambda)
  transformedsignal2 <- padperimeter(vSample = yData, lambda = lambda)
  transformedsignal3 <- padperimeter(vSample = zData, lambda = lambda)
  sampledata <- array(c(transformedsignal1, transformedsignal2, transformedsignal3),
                      dim = c(nrow(transformedsignal1), ncol(transformedsignal1), 3))

  nn <- nrow(transformedsignal1)
  mm <- ncol(transformedsignal1)
  rm(transformedsignal1, transformedsignal2, transformedsignal3)
  # Calculate the number of cores

  if (is.null(no_cores)) {
    no_cores <- parallel::detectCores() - 1 }
  # Initiate cluster
  cl <- parallel::makeCluster(no_cores)

  parallel::clusterExport(cl, c("nn", "mm", "lambda", "sampledata", "index", "FRich", "FEve", "FDiv", "m_radiusindices", "dbFD"), envir = environment())
  test <- sapply((lambda + 1):(nn - lambda),
                 function(x) {
                   parallel::parSapply(cl, (lambda+1):(mm - lambda),
                                                        function(y) {
                                                          if (any(is.na(sampledata[x, y, ]))) { return( NA ) }

                                                          xtmp <- seq(x-lambda, x+lambda)
                                                          ytmp <- seq(y-lambda, y+lambda)
                                                          tmp <- sampledata[xtmp, ytmp, ]
                                                          tmpM <- cbind(c(tmp[,,1])[c(m_radiusindices)], c(tmp[,,2])[c(m_radiusindices)], c(tmp[,,3])[c(m_radiusindices)])
                                                          tmpM <- tmpM[!rowSums(!is.finite(tmpM)), ]               # only lines w/o NAs
                                                          # rownames(tmpM) <- as.character(1:nrow(tmpM))

                                                          switch (index,
                                                            "mean" = tryCatch(mean(tmpM, na.rm = TRUE), error = function(e) { return(NA) } ),
                                                            "richness" = tryCatch(FRich(tmpM), error = function(e) { return(NA) } ),
                                                            "evenness" = tryCatch(FEve(tmpM), error = function(e) { return(NA) }),
                                                            "divergence" = tryCatch(FDiv(tmpM), error = function(e) { return(NA) }),
                                                            "dbFD" = tryCatch(FD::dbFD(tmpM), error = function(e) { return(NA) }))
                                                         }) }, simplify = TRUE)
  parallel::stopCluster(cl)

  return(test)
}

